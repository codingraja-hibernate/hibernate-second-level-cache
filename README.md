# Create Hibernate Application
[![N|Solid](http://www.innerrange.com/Portals/0/LayerSlider3D/606/redbutton_1_2_4.png)](http://codingraja.com/artcles/hibernate)

# Step-1 : Create Maven Project
### Select Maven Archetypes 
  - maven-archetype-quickstart 1.1
  
### Enter a Group Id and Artifact Id 
  - Group Id : com.codingraja
  - Artifact Id : hibernate-second-level-cache

# Step-2 : Add Dependencies in pom.xml
  Open pom.xml file from your project and add these Dependencies

  - Hibernate Dependency
  
  ```<dependency>
		<groupId>org.hibernate</groupId>
		<artifactId>hibernate-core</artifactId>
		<version>5.1.0.Final</version>
	</dependency>
  ```
  
  - DataBase Dependency(MySQL)
  
  ```<dependency>
		<groupId>mysql</groupId>
		<artifactId>mysql-connector-java</artifactId>
		<version>5.1.6</version>
	</dependency>
  ```
  
# Step-3 : Create Entity Classes

Create Entity class inside the src/main/java source folder
	
	- All fields should be private 
	- Create a public no-arguments constructor
	- Create setter and getter methods

# Step-4 : Create Hibernate Mapping Files For Entities
Create EntityClassName.hbm.xml file inside src/main/resources(If resources folder is not available than you should have to create the resources folder).

### There are two ways to create Hibernate Mapping
  - By Using XML
  - By Using Annotation

# Create Hibernate Configuration File
Create hibernate.cfg.xml file inside src/main/resources.
	
	- Write DataSource Details( Driver Class, URL, USERNAME and PASSWORD)
	- Write Hibernate properties
	- Write Resource Mapping files
			
# Create Database (hibernate_db)
   - Open your DB Terminal
  ```
  	create database hibernate_db;
  ```
	
# Create Test Class
Create Test Class to operate the CRUD Operations 